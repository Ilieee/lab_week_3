# Sistema de Gerenciamento de Músicas

Este é um sistema desenvolvido em Spring Boot para gerenciamento de músicas. Ele oferece as seguintes funcionalidades:

- **Adicionar uma Nova Música:** Permite adicionar uma nova música fornecendo informações como nome, artista, gênero musical, duração e descrição.
- **Listar Todas as Músicas:** Permite listar todas as músicas cadastradas.
- **Excluir uma Música:** Permite excluir uma música com base no seu ID.
- **Atualizar os Detalhes de uma Música:** Permite atualizar os detalhes de uma música existente.
- **Pesquisar Músicas por Nome:** Permite pesquisar músicas pelo seu nome.
- **Pesquisar Músicas por Gênero:** Permite pesquisar músicas por gênero musical.
- **Pesquisar Músicas por Tempo de Duração:** Permite pesquisar músicas por tempo de duração.

Este sistema facilita o gerenciamento de um catálogo de músicas, fornecendo operações básicas de CRUD (criar, ler, atualizar, excluir) e pesquisa.
