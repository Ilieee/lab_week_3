package com.Lab3_SD3.Market_Place.Controllers;

import com.Lab3_SD3.Market_Place.Dtos.MusicDto;
import com.Lab3_SD3.Market_Place.Entities.Music;
import com.Lab3_SD3.Market_Place.Repositories.MusicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class MusicController {

    @Autowired
    private MusicRepository repository;

    // Endpoint para adicionar uma nova música
    @PostMapping("/musics")
    public ResponseEntity<Music> create(@RequestBody MusicDto dto) {
        Music music = new Music();
        music.setName(dto.name);
        music.setArtist(dto.artist);
        music.setMusicalGenre(dto.musicalGenre);
        music.setDuration(dto.duration);
        music.setDescription(dto.description);

        repository.save(music);
        return ResponseEntity.status(HttpStatus.CREATED).body(repository.save(music));
    }

    // Endpoint para listar todas as músicas
    @GetMapping("/musics")
    public ResponseEntity<List<Music>> getAllMusics() {
        List<Music> musics = repository.findAll();
        return ResponseEntity.ok().body(musics);
    }

    // Endpoint para excluir uma música por ID
    @DeleteMapping("/musics/{id}")
    public ResponseEntity<?> deleteMusic(@PathVariable UUID id) {
        if (repository.existsById(id)) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    // Endpoint para atualizar os detalhes de uma música existente por ID
    @PutMapping("/musics/{id}")
    public ResponseEntity<Music> updateMusic(@PathVariable UUID id, @RequestBody MusicDto dto) {
        if (repository.existsById(id)) {
            Music music = repository.getOne(id);
            music.setName(dto.name);
            music.setArtist(dto.artist);
            music.setMusicalGenre(dto.musicalGenre);
            music.setDuration(dto.duration);
            music.setDescription(dto.description);
            repository.save(music);
            return ResponseEntity.ok().body(music);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    // Endpoint para consultar músicas por nome
    @GetMapping("/musics/searchByName")
    public ResponseEntity<List<Music>> searchByName(@RequestParam String name) {
        List<Music> musics = repository.findByNameContaining(name);
        return ResponseEntity.ok().body(musics);
    }

    // Endpoint para consultar músicas por gênero
    @GetMapping("/musics/searchByGenre")
    public ResponseEntity<List<Music>> searchByGenre(@RequestParam String genre) {
        List<Music> musics = repository.findByMusicalGenreContaining(genre);
        return ResponseEntity.ok().body(musics);
    }


    // Endpoint para consultar músicas por duração
    @GetMapping("/musics/searchByDuration")
    public ResponseEntity<List<Music>> searchByDuration(@RequestParam double minDuration, @RequestParam double maxDuration) {
        List<Music> musics = repository.findByDurationBetween(minDuration, maxDuration);
        return ResponseEntity.ok().body(musics);
    }
}
