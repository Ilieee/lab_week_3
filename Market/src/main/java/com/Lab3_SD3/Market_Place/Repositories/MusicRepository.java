package com.Lab3_SD3.Market_Place.Repositories;

import com.Lab3_SD3.Market_Place.Entities.Music;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MusicRepository extends JpaRepository<Music, UUID> {
    List<Music> findByNameContaining(String name);
    List<Music> findByMusicalGenreContaining(String musicalGenre);
    List<Music> findByDurationBetween(double minDuration, double maxDuration);
}