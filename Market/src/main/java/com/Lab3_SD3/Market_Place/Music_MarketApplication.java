package com.Lab3_SD3.Market_Place;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Music_MarketApplication {

	public static void main(String[] args) {
		SpringApplication.run(Music_MarketApplication.class, args);
	}

}
