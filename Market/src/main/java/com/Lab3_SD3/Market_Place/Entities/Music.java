package com.Lab3_SD3.Market_Place.Entities;

import jakarta.persistence.*;

@Entity
@Table(name="TB_MUSIC_MARKET")
public class Music extends AbstractEntity {

    @Column(name = "NM_PRODUCT")
    private String name;
    @Column(name = "NM_ARTIST")
    private String artist;
    @Column(name = "DS_MUSICALGENRE")
    private String musicalGenre;

    @Column(name = "DS_DESCRIPTION")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "NR_DURATION")
    private double duration;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getMusicalGenre() {
        return musicalGenre;
    }

    public void setMusicalGenre(String musicalGenre) {
        this.musicalGenre = musicalGenre;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }
}


