package com.Lab3_SD3.Market_Place.Entities;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;
import jakarta.persistence.*;

@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

    public AbstractEntity(){
        Date now = new Date();
        this.CreatedAt = now;
        this.UpdatedAt = now;
        this.Activate = true;

    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID")
    private UUID id;
    @Column(name = "DT_CREATED_AT")
    private final Date CreatedAt;
    @Column(name = "DT_UPDATED_AT")
    private Date UpdatedAt;
    @Column(name="ST_ACTIVE")
    private Boolean Activate;


    public UUID getId() {
        return id;
    }

    public Date getCreatedAt() {
        return CreatedAt;
    }

    public Date getUpdatedAt() {
        return UpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        UpdatedAt = updatedAt;
    }

    public Boolean getActivate() {
        return Activate;
    }

    public void setActivate(Boolean activate) {
        Activate = activate;
    }
}
