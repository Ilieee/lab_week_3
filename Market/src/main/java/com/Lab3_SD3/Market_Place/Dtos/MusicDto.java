package com.Lab3_SD3.Market_Place.Dtos;

public class MusicDto {

    public String name;
    public String artist;
    public String musicalGenre;

    public String description;
    public double duration;
}
